/*
 * model.cpp
 *
 *  Created on: Jan 13, 2022
 *      Author: ben
 */


#include "model.hpp"

namespace cg {

model::model(const char* model_path, const char* texture_path)
:	model_matrix(glm::mat4(1)),
	mvp(glm::mat4(1)),
	texture(loadDDS(texture_path))
{
	bool res = loadOBJ(model_path, vertices, uvs, normals);

	// vertex buffer
	glGenBuffers(1, &vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	// uv buffer
	glGenBuffers(1, &uv_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, uv_buffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

	// normal buffer
	glGenBuffers(1, &normal_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, normal_buffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

}

void model::rotate(float x, float y, float z)
{
	model_matrix = glm::rotate(model_matrix,glm::radians(x),glm::vec3(1,0,0));//rotation x = 0.0 degrees
	model_matrix = glm::rotate(model_matrix,glm::radians(y),glm::vec3(0,1,0));//rotation y = 0.0 degrees
	model_matrix = glm::rotate(model_matrix,glm::radians(z),glm::vec3(0,0,1));//rotation z = 0.0 degrees
}

void model::render(glm::vec3 position, cg::camera& cam)
{

	model_matrix = glm::mat4(1);
	model_matrix = glm::translate(model_matrix, glm::vec3(position[0], position[1], -position[2]));
	rotate(-145.0f, 0.0f, 180.0f);
	mvp = cam.projection_matrix * cam.view_matrix * model_matrix;
	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uv_buffer);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// 3rd attribute buffer : normals
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, normal_buffer);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	// draw triangles
	glDrawArrays(GL_TRIANGLES, 0, vertices.size());
}

}
