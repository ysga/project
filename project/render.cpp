/*
 * render.cpp
 *
 *  Created on: Jan 13, 2022
 *      Author: ben
 */

#include "render.hpp"

namespace cg {

render::render()
{
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	programID = LoadShaders( "StandardShading.vertexshader", "StandardShading.fragmentshader" );

	// Get a handle for our "MVP" uniform
	MatrixID = glGetUniformLocation(programID, "MVP");
	ViewMatrixID = glGetUniformLocation(programID, "V");
	ModelMatrixID = glGetUniformLocation(programID, "M");

	// Get a handle for our "myTextureSampler" uniform
	TextureID  = glGetUniformLocation(programID, "myTextureSampler");

	// Get a handle for our "LightPosition" uniform
	glUseProgram(programID);
	LightID = glGetUniformLocation(programID, "LightPosition_worldspace");

	// settings
	// green background
	glClearColor(0.3f, 0.7f, 0.0f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);
}

render::~render()
{
	// cleanup
	glDeleteProgram(programID);
	glDeleteVertexArrays(1, &VertexArrayID);
}

// apply given rotation to model
void render::rotate(cg::model* mod, float x, float y, float z)
{
	mod->model_matrix = glm::rotate(mod->model_matrix,glm::radians(x),glm::vec3(1,0,0));//rotation x = 0.0 degrees
	mod->model_matrix = glm::rotate(mod->model_matrix,glm::radians(y),glm::vec3(0,1,0));//rotation y = 0.0 degrees
	mod->model_matrix = glm::rotate(mod->model_matrix,glm::radians(z),glm::vec3(0,0,1));//rotation z = 0.0 degrees
}

// apply given rotation to model
void render::rotate(cg::model* mod, glm::vec3 rotation)
{
	mod->model_matrix = glm::rotate(mod->model_matrix,glm::radians(rotation.x),glm::vec3(1,0,0));
	mod->model_matrix = glm::rotate(mod->model_matrix,glm::radians(rotation.y),glm::vec3(0,1,0));
	mod->model_matrix = glm::rotate(mod->model_matrix,glm::radians(rotation.z),glm::vec3(0,0,1));
}

void render::model(cg::model* mod, glm::vec3 position, cg::camera& cam, glm::vec3 rotation)
{

	// Send our transformation to the currently bound shader,
	// in the "MVP" uniform
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mod->mvp[0][0]);
	glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &mod->model_matrix[0][0]);
	glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &cam.view_matrix[0][0]);

	// update model matrix
	mod->model_matrix = glm::mat4(1);
	mod->model_matrix = glm::translate(mod->model_matrix, glm::vec3(position[0], position[1], -position[2]));
	rotate(mod, rotation);
	mod->mvp = cam.projection_matrix*cam.view_matrix*mod->model_matrix;
	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, mod->vertex_buffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, mod->uv_buffer);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// 3rd attribute buffer : normals
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, mod->normal_buffer);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mod->texture);

	// draw triangles
	glDrawArrays(GL_TRIANGLES, 0, mod->vertices.size());
}

// begin rendering
void render::begin(glm::vec3 focus)
{
	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// Use our shader
	glUseProgram(programID);

	glUniform3f(LightID, focus.x, focus.y, 100.0f);


	glUniform1i(TextureID, 0);

}

// end rendering
void render::end(cg::window& window)
{
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);

	// Swap buffers
	glfwSwapBuffers(window.win);
	glfwPollEvents();
}

}

