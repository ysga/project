/*
 * camera.hpp
 *
 *  Created on: Jan 12, 2022
 *      Author: ben
 */

#ifndef PROJECT_CAMERA_HPP_
#define PROJECT_CAMERA_HPP_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>

namespace cg {

struct camera {
	float fov;
	float zoom;
	int window_width;
	int window_height;
	glm::mat4 view_matrix;
	glm::mat4 projection_matrix;
	glm::vec3 position;//   = glm::vec3(0.0f, 0.0f,  50.0f);
	glm::vec3 front;// = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 up;//    = glm::vec3(0.0f, 1.0f,  0.0f);


	camera(int window_width, int window_height, glm::vec3 pos);
	void update(glm::vec3& target);

};

}

#endif /* PROJECT_CAMERA_HPP_ */
