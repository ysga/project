/*
 * enemy.hpp
 *
 *  Created on: Jan 18, 2022
 *      Author: ben
 */

#ifndef PROJECT_ENEMY_HPP_
#define PROJECT_ENEMY_HPP_

#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace cg {

struct enemy {

	enemy(std::string nm, float x, float y, float sp);

	void follow(glm::vec3 target);

	std::string name;
	glm::vec3 position;
	glm::vec3 destination;
	float speed;
	float rotation;
	int lives;
};

}



#endif /* PROJECT_ENEMY_HPP_ */
