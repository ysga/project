/*
 * render.hpp
 *
 *  Created on: Jan 13, 2022
 *      Author: ben
 */

#ifndef PROJECT_RENDER_HPP_
#define PROJECT_RENDER_HPP_

#include <vector>
// Include GLEW
#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>

#include <common/shader.hpp>
#include <common/texture.hpp>
#include <common/objloader.hpp>
#include <common/vboindexer.hpp>

#include "camera.hpp"
#include "model.hpp"
#include "window.hpp"

namespace cg {

struct render {


	GLuint VertexArrayID;

	// Create and compile our GLSL program from the shaders
	GLuint programID;

	// Get a handle for our "MVP" uniform
	GLuint MatrixID;
	GLuint ViewMatrixID;
	GLuint ModelMatrixID;

	// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID;

	// Get a handle for our "LightPosition" uniform
	GLuint LightID;

	render();
	~render();

	void rotate(cg::model* mod, float x, float y, float z);
	void rotate(cg::model* mod, glm::vec3 rotation);
	void model(cg::model* mod, glm::vec3 position,cg::camera& cam, glm::vec3 rotation = glm::vec3(-145.0f, 180.0f, 180.0f));
	void begin(glm::vec3 focus);
	void end(cg::window& window);
};

}

#endif /* PROJECT_RENDER_HPP_ */
