/*
 * enemy.cpp
 *
 *  Created on: Jan 18, 2022
 *      Author: ben
 */

#include "enemy.hpp"

namespace cg {

enemy::enemy(std::string nm, float x, float y, float sp)
:	name(nm),
	position(glm::vec3(x, y, 0.0f)),
	destination(glm::vec3(0)),
	speed(sp),
	rotation(180.0f),
	lives(5)
{}

void enemy::follow(glm::vec3 target)
{
	// get distance between enemy and player
	float dist = glm::distance
					(
							glm::vec2(target.x, target.y),
							glm::vec2(position.x, position.y)
					);

	// follow player if dist >= 3 && dist <= 15
	if (dist >= 3 && dist <= 15)
	{
		destination = target - position;
		destination = glm::normalize(destination) * speed;

		position += destination;
		rotation = atan2(destination.x, destination.y)/0.01745329252f;
	}
}

}


