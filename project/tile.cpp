/*
 * tile.cpp
 *
 *  Created on: Jan 18, 2022
 *      Author: ben
 */

#include "tile.hpp"


namespace cg {

tile::tile(std::string nm, float x, float y)
:	name(nm),
	position(glm::vec3(x, y, 0))
{}


}
