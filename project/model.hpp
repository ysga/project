/*
 * model.hpp
 *
 *  Created on: Jan 13, 2022
 *      Author: ben
 */

#ifndef PROJECT_MODEL_HPP_
#define PROJECT_MODEL_HPP_

#include <vector>
// Include GLEW
#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>

#include <common/shader.hpp>
#include <common/texture.hpp>
#include <common/objloader.hpp>
#include <common/vboindexer.hpp>

#include "camera.hpp"

namespace cg {

struct model {
	glm::mat4 model_matrix;
	glm::mat4 mvp;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	// model
	GLuint vertex_buffer;
	GLuint uv_buffer;
	GLuint normal_buffer;
	// texture
	// Load the texture
	GLuint texture;


	model(const char* model_path, const char* texture_path);
	void rotate(float x, float y, float z);
	void render(glm::vec3 position, cg::camera& cam);
	void render(){}
};

}




#endif /* PROJECT_MODEL_HPP_ */
