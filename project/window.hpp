/*
 * window.hpp
 *
 *  Created on: Jan 13, 2022
 *      Author: ben
 */

#ifndef PROJECT_WINDOW_HPP_
#define PROJECT_WINDOW_HPP_

#include <stdio.h>
#include <stdlib.h>

// Include GLEW
#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>

namespace cg {

struct window {

	GLFWwindow* win;

	window(const char* name, int width, int height);
	~window();

	void set_input();
	GLFWwindow* get();

};

}


#endif /* PROJECT_WINDOW_HPP_ */
