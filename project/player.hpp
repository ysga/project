/*
 * player.hpp
 *
 *  Created on: Jan 12, 2022
 *      Author: ben
 */

#ifndef PROJECT_PLAYER_HPP_
#define PROJECT_PLAYER_HPP_

#include <list>
#include <iostream>

#include "enemy.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>


namespace cg {

struct player {
	glm::vec3 position;
	glm::vec2 direction;
	float rotation;
	int lives;
	bool attacking;

	player();

	void move(GLFWwindow* window);
	void attack(std::list<cg::enemy>& enemies);
};

}


#endif /* PROJECT_PLAYER_HPP_ */
