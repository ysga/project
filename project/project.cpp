// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <map>
#include <iostream>
#include <cmath>
#include <string>
#include <list>
#include <vector>
#include <random>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
//GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// opengl-tutorial
#include <common/shader.hpp>
#include <common/texture.hpp>
#include <common/controls.hpp>
#include <common/objloader.hpp>
#include <common/vboindexer.hpp>

// project
#include "window.hpp"
#include "camera.hpp"
#include "model.hpp"
#include "player.hpp"
#include "render.hpp"
#include "enemy.hpp"
#include "tile.hpp"

int main( void )
{
	// 4 : 3
	cg::window win = cg::window("Project", 128*12, 128*9);
	win.set_input();

	std::map<std::string, cg::model*> models;
	models.emplace("player", new cg::model("models-textures/player.obj", "models-textures/player.DDS"));
	models.emplace("bat", new cg::model("models-textures/bat.obj", "models-textures/bat.DDS"));
	models.emplace("bomb", new cg::model("models-textures/bomb.obj", "models-textures/bomb.DDS"));
	models.emplace("tree", new cg::model("models-textures/tree.obj", "models-textures/tree.DDS"));
	models.emplace("rock", new cg::model("models-textures/rock.obj", "models-textures/rock.DDS"));
	models.emplace("attack", new cg::model("models-textures/attack.obj", "models-textures/attack.DDS"));



	// create camera
	cg::camera camera = cg::camera(1024, 768, glm::vec3(0.0f, 0.0f, 50.0f));

	// renderer
	cg::render render = cg::render();

	// create player
	cg::player player = cg::player();

	// random number range for position and speed of the enemies
    static std::default_random_engine rnd;
    static std::uniform_real_distribution<> dis(-30, 30);
    static std::uniform_real_distribution<> dis2(0.03, 0.15);

    // create enemy vector
	std::vector<cg::tile> map;

	// add trees and rocks to the vector with random position
	for(int i = 0; i < 5; ++i)
	{
		map.emplace_back(cg::tile("tree", dis(rnd), dis(rnd)));
		map.emplace_back(cg::tile("rock", dis(rnd), dis(rnd)));
	}

	// create enemy list
	std::list<cg::enemy> enemies;


	// add bats and bombs to the list with random position and speed
	for(int i = 0; i < 30; ++i)
	{
		enemies.push_back(cg::enemy("bat", dis(rnd), dis(rnd), dis2(rnd)));
	}

	for(int i = 0; i < 10; ++i)
	{
		enemies.push_back(cg::enemy("bomb", dis(rnd), dis(rnd), dis2(rnd)));
	}





	do{
		// end or continue
		if (enemies.size() == 0)
		{
			std::cout << "You win!" << std::endl;
			exit(0);
		}
		else if (player.lives <= 0)
		{
			std::cout << "You lose!" << std::endl;
			exit(0);
		}

		// update
		player.move(win.win);

		// attack enemies
		player.attack(enemies);

		// follow player and erase dead enemies
		for (std::list<cg::enemy>::iterator it = enemies.begin(); it != enemies.end(); ++it)
		{
			it->follow(player.position);
			if (it->lives <= 0)
			{
				it = enemies.erase(it);
			}
		}

		// update camera (follow player)
		camera.update(player.position);

		// render
		render.begin(player.position);

			// render player
			render.model(models["player"], player.position, camera, glm::vec3(-145.0f, player.rotation, 180.0f));


			// render enemies
			for (auto& e : enemies)
			{
				render.model(models[e.name], e.position, camera, glm::vec3(-145.0f, e.rotation, 180.0f));
			}

			for (auto& t : map)
			{
				render.model(models[t.name], t.position, camera, glm::vec3(-145.0f, 180.f, 180.0f));
			}

			// render in front of everything
			glClear(GL_DEPTH_BUFFER_BIT);

			// render attack effect
			if (player.attacking)
			{
				glm::vec3 pos = glm::vec3
						(
								player.position.x + player.direction.x*3,
								player.position.y + player.direction.y*3,
								player.position.z
						);
				if (player.direction.x != 0.0f || player.direction.y != 0.0f)
				{
					render.model(models["attack"], pos, camera, glm::vec3(0.0f, 180.0f, 180.0f));
				}
			}

		render.end(win);

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(win.win, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(win.win) == 0 );


	// Cleanup VBO and shader
	for (auto const& m : models)
	{
		glDeleteBuffers(1, &m.second->vertex_buffer);
		glDeleteBuffers(1, &m.second->uv_buffer);
		glDeleteBuffers(1, &m.second->normal_buffer);
		glDeleteTextures(1, &m.second->texture);
	}


	return 0;
}
