/*
 * player.cpp
 *
 *  Created on: Jan 12, 2022
 *      Author: ben
 */

#include "player.hpp"

namespace cg {

player::player()
:	position(0.0f, 0.0f, 0.0f),
	direction(0.0f, -1.0f),
	rotation(0.0f),
	lives(9999),
	attacking(false)
{}

void player::move(GLFWwindow* window)
{

	attacking = false;
	// move player
	if
	(
			(glfwGetKey( window, GLFW_KEY_UP) == GLFW_PRESS) ||
			(glfwGetKey( window, GLFW_KEY_LEFT) == GLFW_PRESS) ||
			(glfwGetKey( window, GLFW_KEY_DOWN) == GLFW_PRESS) ||
			(glfwGetKey( window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	)
	{
		direction[0] = 0.0f;
		direction[1] = 0.0f;
	}
	if (glfwGetKey( window, GLFW_KEY_RIGHT) == GLFW_PRESS){
		position[0] += 0.2f;
		direction[0] = 1.0f;
	}
	if (glfwGetKey( window, GLFW_KEY_LEFT) == GLFW_PRESS){
		position[0] -= 0.2f;
		direction[0] = -1.0f;
	}
	if (glfwGetKey( window, GLFW_KEY_UP) == GLFW_PRESS){
		position[1] += 0.2f;
		direction[1] = 1.0f;
	}
	if (glfwGetKey( window, GLFW_KEY_DOWN) == GLFW_PRESS){
		position[1] -= 0.2f;
		direction[1] = -1.0f;
	}
	if (glfwGetKey( window, GLFW_KEY_X) == GLFW_PRESS){
		attacking = true;
	}

	// set player rotation
	if (direction[0] > 0.0f && direction[1] == 0.0f) rotation = 90.0f;
	else if (direction[0] > 0.0f && direction[1] > 0.0f) rotation = 45.0f;
	else if (direction[0] == 0.0f && direction[1] > 0.0f) rotation = 0.0f;
	else if (direction[0] < 0.0f && direction[1] > 0.0f) rotation = -45.0f;
	else if (direction[0] < 0.0f && direction[1] == 0.0f) rotation = -90.0f;
	else if (direction[0] < 0.0f && direction[1] < 0.0f) rotation = -135.0f;
	else if (direction[0] == 0.0f && direction[1] < 0.0f) rotation = 180.0f;
	else if (direction[0] > 0.0f && direction[1] < 0.0f) rotation = 135.0f;
}

void player::attack(std::list<cg::enemy>& enemies)
{
	for (auto& e : enemies)
	{
		// get distance between player and enemy
		float dist = glm::distance
				(
						glm::vec2(e.position.x, e.position.y),
						glm::vec2(position.x, position.y)
				);

		// decrease life of enemy
		if (attacking && dist <= 5)
		{
			e.position.x += 3.0f*(-e.rotation)/100;
			e.position.y += 3.0f*(-e.rotation)/100;
			--e.lives;
		}
		if (!attacking && dist <= 5)
		{
			--lives;
		}
	}

}

}


