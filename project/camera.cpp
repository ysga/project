/*
 * camera.cpp
 *
 *  Created on: Jan 12, 2022
 *      Author: ben
 */

#include "camera.hpp"

//glm::vec3 cameraPos   = glm::vec3(0.0f, 0.0f,  50.0f);

namespace cg {

camera::camera(int win_width, int win_height, glm::vec3 pos)
:
	fov(45.0f),
	zoom(5.0f),
	window_width(win_width),
	window_height(win_height),
	view_matrix(glm::mat4(0.0f)),
	projection_matrix(glm::perspective(glm::radians(fov), 1024.0f/ 768.0f, 0.1f, 100.0f)),
	position(pos),
	front(glm::vec3(0.0f, 0.0f, -1.0f)),
	up(glm::vec3(0.0f, 1.0f,  0.0f))
{}

void camera::update(glm::vec3 &target)
{
	// follow target
	position[0] = target[0]; // follow player
	position[1] = target[1]; // follow player

	// orthographic camera
	projection_matrix = glm::ortho
			(
					0.0f-window_width/10.0f/zoom, window_width/10.0f*2/zoom-window_width/10.0f/zoom,
					0.0f-window_height/10.0f/zoom, window_height/10.0f*2/zoom-window_height/10.0f/zoom,
					0.0f, 1000.0f
			);
	// perspective camerea
	//		projection_matrix = glm::perspective(glm::radians(fov), 800.0f / 600.0f, 0.1f, 100.0f);
	view_matrix = glm::lookAt(position, position + front, up);
}


}



