/*
 * tile.hpp
 *
 *  Created on: Jan 18, 2022
 *      Author: ben
 */

#ifndef PROJECT_TILE_HPP_
#define PROJECT_TILE_HPP_

#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace cg {

struct tile {

	tile(std::string nm, float x, float y);

	std::string name;
	glm::vec3 position;
};

}



#endif /* PROJECT_TILE_HPP_ */
