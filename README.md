#### Build and Run:

```
$ git clone https://gitlab.com/ysga/project
$ cd project
$ mkdir build && cd build
$ cmake ../
$ make
$ cd ../project
$ ./project
```

Move: arrow keys

Kill: x key 

Exit: escape key
